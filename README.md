# BEAURIS

 [![pipeline status](https://gitlab.com/beaur1s/beauris/badges/main/pipeline.svg)](https://gitlab.com/beaur1s/beauris/-/commits/main) [![PyPI version](https://badge.fury.io/py/beauris.svg)](https://badge.fury.io/py/beauris) [![Documentation Status](https://readthedocs.org/projects/beauris/badge/?version=latest)](https://beauris.readthedocs.io/en/latest/?badge=latest)

BEAURIS: an automated system for the creation of genome portals

Originally written for genomes hosted by GOGEPP on https://bipaa.genouest.org and https://bbip.genouest.org, as well as ABiMS and SEBIMER.

## Documentation

This repository contains the common code needed to deploy any BEAURIS genome portal. To use it, you will need to create another GitLab repository, following the [provided example](https://gitlab.com/beaur1s/sample).

Check out the [BEAURIS documentation](https://beauris.readthedocs.io/)!

## Authors

BEAURIS was developped initially by:

- GOGEPP/GenOuest (Rennes, France): used on [BIPAA](https://bipaa.genouest.org) and [BBIP](https://bbip.genouest.org)
- ABiMS (Roscoff, France)
- SEBIMER (Brest, France)

See [up-to-date contributors list](https://gitlab.com/beaur1s/beauris/-/graphs/main).
