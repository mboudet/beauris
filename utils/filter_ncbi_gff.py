#!/usr/bin/env python

import argparse
import sys

from BCBio import GFF

from Bio.SeqRecord import SeqRecord


class NCBIFilter():

    def process_gff(self, infile, outfile, outdiscarded):

        recs = []
        recs_discarded = []

        for rec in GFF.parse(infile):
            rec.annotations = {}
            rec.seq = ""
            self.kept_genes = []
            self.discarded = []
            for feat in rec.features:

                if feat.type in ("gene"):
                    if feat.qualifiers['source'][0] == 'Gnomon':
                        sf_kept, sf_discarded = self.filter_subfeats(feat)

                        if len(sf_kept) > 0:
                            feat.sub_features = sf_kept
                            self.kept_genes.append(feat)

                        if len(sf_discarded) > 0:
                            feat.sub_features = sf_discarded
                            print("Discarding gene subfeatures of type(s) {}".format([x.type for x in feat.sub_features]))
                            self.discarded.append(feat)
                    else:
                        print("Discarding gene of source {}".format(feat.qualifiers['source'][0]))
                        self.discarded.append(feat)
                else:
                    print("Discarding feature of type {}".format(feat.type))
                    self.discarded.append(feat)

            rec.features = self.kept_genes
            recs.append(rec)

            recd = SeqRecord("", rec.id)
            recd.features = self.discarded
            recs_discarded.append(recd)

        GFF.write(recs, outfile)

        GFF.write(recs_discarded, outdiscarded)

    def filter_subfeats(self, feat):

        kept = []
        discarded = []
        for sf in feat.sub_features:
            if sf.type in ("mRNA"):
                kept.append(sf)
            else:
                discarded.append(sf)

        return kept, discarded


parser = argparse.ArgumentParser()
parser.add_argument('infile', nargs='?', type=argparse.FileType('r'), default=sys.stdin)
parser.add_argument('outfile', nargs='?', type=argparse.FileType('w'), default=sys.stdout)
parser.add_argument('discarded', type=argparse.FileType('w'))
args = parser.parse_args()

filter = NCBIFilter()
filter.process_gff(args.infile, args.outfile, args.discarded)
