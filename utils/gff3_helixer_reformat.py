#!/usr/bin/env python

# script to (1) change ID of genes/mrna/exons/cds/3'/5' and (2) add Name for genes and mrna, in annotation file (gff) from Helixer

from __future__ import print_function

import argparse
import sys

from BCBio import GFF

parser = argparse.ArgumentParser()
parser.add_argument('ingff', nargs='?', type=argparse.FileType('r'), default=sys.stdin)
parser.add_argument('outfile', nargs='?', type=argparse.FileType('w'), default=sys.stdout)
parser.add_argument("genePrefix", help="The suffix added to id and name gene")  # example SPIC for Spodoptera picta

args = parser.parse_args()

recs = []

id = 0

for rec in GFF.parse(args.ingff):  # rec = chr ou scaffold

    for f in rec.features:  # gene
        id += 1
        # Adding zeros in ID to obtain 6 numbers long
        ID = str(id).zfill(6)

        f.qualifiers["ID"][0] = args.genePrefix + ID
        f.qualifiers["Name"] = f.qualifiers["ID"][0]

        mrnaSuffix = "A"
        for sf in f.sub_features:  # mRNA
            idExon = 0
            idCDS = 0

            sf.qualifiers["Parent"][0] = args.genePrefix + ID
            sf.qualifiers["ID"][0] = args.genePrefix + ID + "-R" + mrnaSuffix
            sf.qualifiers["Name"] = sf.qualifiers["ID"][0]

            # Using chr()+ord() to incremente character
            mrnaSuffix = chr(ord(mrnaSuffix) + 1)

            for ssf in sf.sub_features:  # CDS exon 5prime 3prime
                ssf.qualifiers["Parent"][0] = sf.qualifiers["ID"][0]

                if ssf.type == "exon":
                    idExon += 1
                    ssf.qualifiers["ID"][0] = sf.qualifiers["ID"][0] + "-exon-" + str(idExon)

                elif ssf.type == "CDS":
                    idCDS += 1
                    ssf.qualifiers["ID"][0] = sf.qualifiers["ID"][0] + "-CDS-" + str(idCDS)

                elif ssf.type == "five_prime_UTR":
                    ssf.qualifiers["ID"][0] = sf.qualifiers["ID"][0] + "-five_prime_UTR"

                elif ssf.type == "three_prime_UTR":
                    ssf.qualifiers["ID"][0] = sf.qualifiers["ID"][0] + "-three_prime_UTR"

    recs.append(rec)

GFF.write(recs, args.outfile)
