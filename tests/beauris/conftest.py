import os

from beauris import Beauris

import pytest


@pytest.fixture
def beauris(tmp_path_factory):

    work_dir = tmp_path_factory.mktemp("beauris_tmp")
    bo = Beauris(root_work_dir=work_dir, config_file=os.path.join(os.path.dirname(os.path.realpath(__file__)), '../../test-data/beauris.yml'))

    return bo


@pytest.fixture
def org(tmp_path_factory):

    work_dir = tmp_path_factory.mktemp("beauris_tmp")
    bo = Beauris(root_work_dir=work_dir, config_file=os.path.join(os.path.dirname(os.path.realpath(__file__)), '../../test-data/beauris.yml'))
    org = bo.load_organism(os.path.join(os.path.dirname(os.path.realpath(__file__)), '../../test-data/organisms/test_organism.yml'), test_data=True)

    return org


@pytest.fixture
def org_locked(tmp_path_factory):

    work_dir = tmp_path_factory.mktemp("beauris_tmp")
    bo = Beauris(root_work_dir=work_dir, config_file=os.path.join(os.path.dirname(os.path.realpath(__file__)), '../../test-data/beauris.yml'))
    locked_dir = prepare_lockdir(tmp_path_factory, bo, 'test_organism.yml')

    org = bo.load_organism(os.path.join(os.path.dirname(os.path.realpath(__file__)), '../../test-data/organisms/test_organism.yml'), test_data=True, locked_dir=locked_dir)

    return org


@pytest.fixture
def org_mucor(tmp_path_factory):

    work_dir = tmp_path_factory.mktemp("beauris_tmp")
    bo = Beauris(root_work_dir=work_dir, config_file=os.path.join(os.path.dirname(os.path.realpath(__file__)), '../../test-data/beauris.yml'))
    org = bo.load_organism(os.path.join(os.path.dirname(os.path.realpath(__file__)), '../../test-data/organisms/mucor_mucedo.yml'), test_data=True)

    return org


@pytest.fixture
def org_small(tmp_path_factory):

    work_dir = tmp_path_factory.mktemp("beauris_tmp")

    # Say the lock file is in a non existing dir
    locked_dir = os.path.join(os.path.dirname(os.path.realpath(__file__)), '../../test-data/NOTlocked')
    bo = Beauris(root_work_dir=work_dir, config_file=os.path.join(os.path.dirname(os.path.realpath(__file__)), '../../test-data/beauris.yml'))
    org = bo.load_organism(os.path.join(os.path.dirname(os.path.realpath(__file__)), '../../test-data/organisms/small.yml'), test_data=True, locked_dir=locked_dir)

    return org


@pytest.fixture
def org_services(tmp_path_factory):

    work_dir = tmp_path_factory.mktemp("beauris_tmp")
    bo = Beauris(root_work_dir=work_dir, config_file=os.path.join(os.path.dirname(os.path.realpath(__file__)), '../../test-data/beauris.yml'))
    org = bo.load_organism(os.path.join(os.path.dirname(os.path.realpath(__file__)), '../../test-data/organisms/services.yml'), test_data=True)

    return org


@pytest.fixture
def org_small_locked(tmp_path_factory):

    work_dir = tmp_path_factory.mktemp("beauris_tmp")

    bo = Beauris(root_work_dir=work_dir, config_file=os.path.join(os.path.dirname(os.path.realpath(__file__)), '../../test-data/beauris.yml'))
    locked_dir = prepare_lockdir(tmp_path_factory, bo, 'small.yml')

    org = bo.load_organism(os.path.join(os.path.dirname(os.path.realpath(__file__)), '../../test-data/organisms/small.yml'), test_data=True, locked_dir=locked_dir)

    return org


@pytest.fixture
def locker(tmp_path_factory):

    work_dir = tmp_path_factory.mktemp("beauris_locked_tmp")
    bo = Beauris(root_work_dir=work_dir, config_file=os.path.join(os.path.dirname(os.path.realpath(__file__)), '../../test-data/beauris.yml'))

    locker = bo.get_data_locker(override_conf={'target_dir': str(work_dir)})

    return locker


@pytest.fixture
def fake_file(tmp_path_factory):

    tmp_path = tmp_path_factory.mktemp("data_random") / "something.txt"
    with open(tmp_path, 'w') as tmp_pathh:
        tmp_pathh.write("blabla")

    return tmp_path


@pytest.fixture
def another_fake_file(tmp_path_factory):

    tmp_path = tmp_path_factory.mktemp("data_random") / "something_else.txt"
    with open(tmp_path, 'w') as tmp_pathh:
        tmp_pathh.write("blabla")

    return tmp_path


@pytest.fixture
def tmp_dir(tmp_path_factory):

    tmp_path = tmp_path_factory.mktemp("tmp_dir")

    return tmp_path


@pytest.fixture
def org_small_future_locked(tmp_path_factory):

    work_dir = tmp_path_factory.mktemp("beauris_tmp")

    bo = Beauris(root_work_dir=work_dir, config_file=os.path.join(os.path.dirname(os.path.realpath(__file__)), '../../test-data/beauris.yml'))
    locked_dir = prepare_lockdir(tmp_path_factory, bo, 'small.yml')
    future_locked_dir = prepare_lockdir(tmp_path_factory, bo, 'small.yml', future=True)

    org = bo.load_organism(os.path.join(os.path.dirname(os.path.realpath(__file__)), '../../test-data/organisms/small.yml'), test_data=True, locked_dir=locked_dir, future_locked_dir=future_locked_dir)

    return org


def prepare_lockdir(tmp_path_factory, bo, org_yml, future=False):

    locked_dir = tmp_path_factory.mktemp("beauris_locked_ymls")

    conf = {'target_dir': str(locked_dir), 'locked_yml_dir': str(locked_dir)}

    if future:
        future_locked_dir = tmp_path_factory.mktemp("beauris_locked_ymls_future")
        conf['locked_yml_dir_future'] = future_locked_dir
        locked_dir = future_locked_dir

    locked_dir_src = os.path.join(os.path.dirname(os.path.realpath(__file__)), '../../test-data/locked')
    if future:
        locked_dir_src = os.path.join(os.path.dirname(os.path.realpath(__file__)), '../../test-data/future_locked')

    # Create a fake locked file with correct locked_dir
    with open(os.path.join(locked_dir_src, org_yml), 'r') as inlocked:
        with open(os.path.join(locked_dir, org_yml), 'w') as outlocked:
            locked_txt = inlocked.read()
            locked_txt = locked_txt.replace('$LOCKED_DIR', str(locked_dir))
            outlocked.write(locked_txt)

    return locked_dir
