import os

from . import BeaurisTestCase


class TestOrganism(BeaurisTestCase):

    def test_work_dir(self, org):

        assert org.get_work_dir() == os.path.join(org.config.root_work_dir, "{}brassica_napus".format(org.get_ci_prefix()))

    def test_slug(self, org):

        slug_long = org.slug()
        slug_short = org.slug(short=True)

        assert slug_long == "brassica_napus"
        assert slug_short == "brassica_napus"

    def test_work_dir_without_computer_name(self, org_mucor):

        assert org_mucor.get_work_dir() == os.path.join(org_mucor.config.root_work_dir, "{}mucor_mucedo_muc1_male".format(org_mucor.get_ci_prefix()))

    def test_slug_without_computer_name(self, org_mucor):

        slug_long = org_mucor.slug()
        slug_short = org_mucor.slug(short=True)

        assert slug_long == "mucor_mucedo_muc1_male"
        assert slug_short == "mmucedo"

    def test_deploy_services_default(self, org):

        assert org.get_deploy_services("staging") == ['blast', 'download', 'jbrowse', 'authelia', 'apollo']
        assert org.get_deploy_services("production") == ['blast', 'download', 'jbrowse', 'authelia', 'apollo']

    def test_deploy_services_overriden(self, org_services):

        assert org_services.get_deploy_services("staging") == ['apollo']

    def test_tasks_default(self, org):

        assert list(org.tasks.keys()) == [
            'deploy_perms',
            'deploy_download',
            'deploy_blast',
            'deploy_jbrowse',
        ]

    def test_task_options(self, org_services):

        assert org_services.assemblies[0].annotations[0].get_task_options('ogs_check') == {
            'no_size': True,
            "adopt_rna_suffix": "-RA",
        }

    def test_published_files(self, org):

        published = []

        for file, entity in org.get_files_to_publish():
            published.append(file.get_publish_path(entity.get_work_dir(), 'xx', entity))

        assert published == [
            'xx/brassica_napus/assembly_10/fasta/genome.fa',
            'xx/brassica_napus/assembly_10/annotation_1.5/gff/test.gff',
            'xx/brassica_napus/assembly_10/annotation_1.5/fixed_gff/fixed.gff',
            'xx/brassica_napus/assembly_10/annotation_1.5/cds_fa/brassica_napus_ass10_annot1.5_cds.fa',
            'xx/brassica_napus/assembly_10/annotation_1.5/proteins_fa/brassica_napus_ass10_annot1.5_proteins.fa',
            'xx/brassica_napus/assembly_10/annotation_1.5/transcripts_fa/brassica_napus_ass10_annot1.5_transcripts.fa',
            'xx/brassica_napus/assembly_10/annotation_1.5/blast2go_annot/blast2go.annot',
            'xx/brassica_napus/assembly_10/annotation_1.5/blast2go_gaf/blast2go.gaf',
            'xx/brassica_napus/assembly_10/annotation_1.5/blast2go_pdf/blast2go_report.pdf',
            'xx/brassica_napus/assembly_10/annotation_1.5/diamond/diamond_all.xml',
            'xx/brassica_napus/assembly_10/annotation_1.5/eggnog/eggnog_annotations.tsv',
            'xx/brassica_napus/assembly_10/annotation_1.5/interproscan/interproscan.tsv',
            'xx/brassica_napus/assembly_10/annotation_1.5/func_annot_readme/README',
            'xx/brassica_napus/assembly_10/track_Experiment_X_replicate_2/track_file/file.bam',
            'xx/brassica_napus/assembly_10/track_Experiment_X_replicate_2_dna/track_file/file.bam',
            'xx/brassica_napus/assembly_10/track_Repeats_from_REPET/track_file/repeats.gff',
            'xx/brassica_napus/assembly_4.1/fasta/genome.fa',
        ]
