import os

from beauris.util import Util, find_mr_labels

import yaml

from . import BeaurisTestCase


class TestLock(BeaurisTestCase):

    def test_lock_yml(self, org_small, locker, fake_file):

        """
        Lock an organism that has never been locked before
        """

        self.create_fake_derived_files(org_small, locker.target_dir)

        org_small.lock(locker, recursive=True)

        with open(os.path.join(os.path.dirname(os.path.realpath(__file__)), '../../test-data/locked/small.yml'), 'r') as locked:
            locked_txt = locked.read()

        input_root = os.path.abspath(os.path.join(os.path.dirname(os.path.realpath(__file__)), '../../test-data'))
        derived_root = locker.target_dir  # We know it's a DirLocker

        locked_txt = locked_txt.replace('$TEST_DATA_DIR', input_root)
        locked_txt = locked_txt.replace('$LOCKED_DIR', derived_root)

        assert org_small.get_locked_yml() == yaml.safe_load(locked_txt)

        # Check that the assembly file is in correct shape
        assembly_file = org_small.assemblies[0].input_files['fasta']
        assert assembly_file.get_revision() == 0
        assert assembly_file.locked_revision is None
        assert assembly_file.locked_path.startswith(derived_root)
        assert assembly_file.get_locked_path().startswith(derived_root)

        assert assembly_file.get_usable_path() == input_root + '/data/genome.fa'

        assert assembly_file.has_changed_since_last_lock()

        # Modify the input file path and see what happens
        assembly_file.path = fake_file

        assert assembly_file.get_usable_path() == fake_file

    def test_lock_yml_already_locked(self, org_small_locked, fake_file):

        """
        Lock an organism that has been locked once before
        """

        locker = self.get_locker(org_small_locked.locked_dir)

        self.create_fake_derived_files(org_small_locked, locker.target_dir)

        org_small_locked.lock(locker, recursive=True)

        with open(os.path.join(os.path.dirname(os.path.realpath(__file__)), '../../test-data/locked/small.yml'), 'r') as locked:
            locked_txt = locked.read()

        input_root = os.path.abspath(os.path.join(os.path.dirname(os.path.realpath(__file__)), '../../test-data'))
        # derived_root = locker.target_dir  # We know it's a DirLocker

        locked_txt = locked_txt.replace('$TEST_DATA_DIR', input_root)
        locked_txt = locked_txt.replace('$LOCKED_DIR', locker.target_dir)

        assert org_small_locked.get_locked_yml() == yaml.safe_load(locked_txt)

        # Check that the assembly file is in correct shape
        assembly_file = org_small_locked.assemblies[0].input_files['fasta']
        assert assembly_file.get_revision() == 0
        assert assembly_file.locked_revision == 0
        assert assembly_file.locked_path == '$LOCKED_DIR/Blabla/Blobloblo/GOGEPP5/10/fasta/10/genome.fa'.replace('$LOCKED_DIR', locker.target_dir)
        assert assembly_file.get_locked_path() == '$LOCKED_DIR/Blabla/Blobloblo/GOGEPP5/10/fasta/10/genome.fa'.replace('$LOCKED_DIR', locker.target_dir)

        assert assembly_file.get_usable_path() == input_root + '/data/genome.fa'

        assert not assembly_file.has_changed_since_last_lock()

    def test_lock_yml_already_locked_change(self, org_small_locked, fake_file):

        locker = self.get_locker(org_small_locked.locked_dir)

        self.create_fake_derived_files(org_small_locked, locker.target_dir)

        org_small_locked.lock(locker, recursive=True)

        with open(os.path.join(os.path.dirname(os.path.realpath(__file__)), '../../test-data/locked/small.yml'), 'r') as locked:
            locked_txt = locked.read()

        input_root = os.path.abspath(os.path.join(os.path.dirname(os.path.realpath(__file__)), '../../test-data'))

        locked_txt = locked_txt.replace('$TEST_DATA_DIR', input_root)
        locked_txt = locked_txt.replace('$LOCKED_DIR', locker.target_dir)

        assert org_small_locked.get_locked_yml() == yaml.safe_load(locked_txt)

        assembly_file = org_small_locked.assemblies[0].input_files['fasta']

        # Simulate a change in an input file
        assembly_file.path = fake_file
        assembly_file.revision = assembly_file.get_revision() + 1

        assert assembly_file.has_changed_since_last_lock()

        assert assembly_file.get_usable_path() == fake_file

        with open(os.path.join(os.path.dirname(os.path.realpath(__file__)), '../../test-data/locked/small_after_assembly_bump.yml'), 'r') as locked:
            locked_txt_bump = locked.read()
        locked_txt_bump = locked_txt_bump.replace('$TEST_DATA_DIR', input_root)
        locked_txt_bump = locked_txt_bump.replace('$LOCKED_DIR', locker.target_dir)
        exp_locked_yml = yaml.safe_load(locked_txt_bump)

        exp_locked_yml['assemblies'][0]['file']['path'] = fake_file
        assert org_small_locked.get_locked_yml() == exp_locked_yml

    def test_lock_yml_already_locked_change_derived(self, org_small_locked, fake_file):

        locker = self.get_locker(org_small_locked.locked_dir)

        self.create_fake_derived_files(org_small_locked, locker.target_dir)

        org_small_locked.lock(locker, recursive=True)

        with open(os.path.join(os.path.dirname(os.path.realpath(__file__)), '../../test-data/locked/small.yml'), 'r') as locked:
            locked_txt = locked.read()

        input_root = os.path.abspath(os.path.join(os.path.dirname(os.path.realpath(__file__)), '../../test-data'))

        locked_txt = locked_txt.replace('$TEST_DATA_DIR', input_root)
        locked_txt = locked_txt.replace('$LOCKED_DIR', locker.target_dir)

        assert org_small_locked.get_locked_yml() == yaml.safe_load(locked_txt)

        # Simulate a change in a derived file
        fixed_gff_file = org_small_locked.assemblies[0].annotations[0].derived_files['fixed_gff']
        fixed_gff_file.path = fake_file
        fixed_gff_file.set_dirty()

        assert fixed_gff_file.has_changed_since_last_lock()

        assert fixed_gff_file.get_usable_path(force_work_dir=True) == fake_file

        with open(os.path.join(os.path.dirname(os.path.realpath(__file__)), '../../test-data/locked/small_after_fixedgff_bump.yml'), 'r') as locked:
            locked_txt_bump = locked.read()
        locked_txt_bump = locked_txt_bump.replace('$TEST_DATA_DIR', input_root)
        locked_txt_bump = locked_txt_bump.replace('$LOCKED_DIR', locker.target_dir)
        exp_locked_yml = yaml.safe_load(locked_txt_bump)

        assert org_small_locked.get_locked_yml() == exp_locked_yml

    def test_lock_track_derived(self, org_locked):

        """
        Lock an organism that has some tracks
        """
        locker = self.get_locker(org_locked.locked_dir)

        self.create_fake_input_files(org_locked, locker.target_dir)
        self.create_fake_derived_files(org_locked, locker.target_dir)

        org_locked.lock(locker, recursive=True)

        with open(os.path.join(os.path.dirname(os.path.realpath(__file__)), '../../test-data/locked/test_organism.yml'), 'r') as locked:
            locked_txt = locked.read()

        input_root = os.path.abspath(os.path.join(os.path.dirname(os.path.realpath(__file__)), '../../test-data'))
        derived_root = locker.target_dir  # We know it's a DirLocker

        locked_txt = locked_txt.replace('$TEST_DATA_DIR', input_root)
        locked_txt = locked_txt.replace('$LOCKED_DIR', derived_root)

        src_locked_yml = yaml.safe_load(locked_txt)

        assert org_locked.get_locked_yml() == src_locked_yml

        ass = org_locked.get_assembly('10')

        assert len(ass.tracks) == 3

        rnaseq = ass.get_track('Experiment X replicate 2')
        dnaseq = ass.get_track('Experiment X replicate 2 dna')

        expected_locked_bam = src_locked_yml['assemblies'][0]['tracks'][0]['file']['locked_path']
        expected_locked_bam_d = src_locked_yml['assemblies'][0]['tracks'][1]['file']['locked_path']
        expected_locked_bai = src_locked_yml['assemblies'][0]['tracks'][0]['derived'][0]['locked_path']
        expected_locked_bai_d = src_locked_yml['assemblies'][0]['tracks'][1]['derived'][0]['locked_path']
        expected_locked_wig = src_locked_yml['assemblies'][0]['tracks'][0]['derived'][1]['locked_path']
        expected_locked_wig_d = src_locked_yml['assemblies'][0]['tracks'][1]['derived'][1]['locked_path']
        expected_locked_gff = src_locked_yml['assemblies'][0]['tracks'][2]['file']['locked_path']

        expected_paths = {
            'bai': {
                ('RNASeq', 'Experiment X replicate 2'): expected_locked_bai,
                ('DNASeq', 'Experiment X replicate 2 dna'): expected_locked_bai_d
            },
            'bam': {
                ('RNASeq', 'Experiment X replicate 2'): expected_locked_bam,
                ('DNASeq', 'Experiment X replicate 2 dna'): expected_locked_bam_d
            },
            'gff': {
                ('gff', 'Repeats from REPET'): expected_locked_gff
            },
            'wig': {
                ('RNASeq Coverage', 'Experiment X replicate 2'): expected_locked_wig,
                ('DNASeq Coverage', 'Experiment X replicate 2 dna'): expected_locked_wig_d
            }
        }

        assert ass.get_track_paths() == expected_paths

        wig_file = rnaseq.derived_files['wig']
        bai_file = rnaseq.derived_files['bai']
        wig_file_d = dnaseq.derived_files['wig']
        bai_file_d = dnaseq.derived_files['bai']

        assert wig_file.locked_path == expected_locked_wig
        assert wig_file.get_locked_path() == expected_locked_wig
        assert wig_file.get_usable_path() == expected_locked_wig
        assert wig_file_d.locked_path == expected_locked_wig_d
        assert wig_file_d.get_locked_path() == expected_locked_wig_d
        assert wig_file_d.get_usable_path() == expected_locked_wig_d

        fake_jbrowse_json = [
            {
                'storeClass': 'JBrowse/Store/SeqFeature/BAM',
                'urlTemplate': 'test/path/somewhere_bam',
                'category': 'RNASeq',
                'key': 'Experiment X replicate 2',
            },
            {
                'storeClass': "JBrowse/Store/SeqFeature/BigWig",
                'urlTemplate': 'test/path/somewhere_wig',
                'category': 'RNASeq Coverage',
                'key': 'Experiment X replicate 2',
            },
            {
                'storeClass': 'JBrowse/Store/SeqFeature/BAM',
                'urlTemplate': 'test/path/somewhere_else_bam',
                'category': 'DNASeq',
                'key': 'Experiment X replicate 2 dna',
            },
            {
                'storeClass': "JBrowse/Store/SeqFeature/BigWig",
                'urlTemplate': 'test/path/somewhere_else_wig',
                'category': 'DNASeq Coverage',
                'key': 'Experiment X replicate 2 dna',
            },
        ]
        expected_track_paths = {
            'bai': {
                ('RNASeq', 'Experiment X replicate 2'): src_locked_yml['assemblies'][0]['tracks'][0]['derived'][0]['locked_path'],
                ('DNASeq', 'Experiment X replicate 2 dna'): src_locked_yml['assemblies'][0]['tracks'][1]['derived'][0]['locked_path'],
            },
            'bam': {
                ('RNASeq', 'Experiment X replicate 2'): src_locked_yml['assemblies'][0]['tracks'][0]['file']['locked_path'],
                ('DNASeq', 'Experiment X replicate 2 dna'): src_locked_yml['assemblies'][0]['tracks'][1]['file']['locked_path'],
            },
            'gff': {('gff', 'Repeats from REPET'): src_locked_yml['assemblies'][0]['tracks'][2]['file']['locked_path']},
            'wig': {
                ('RNASeq Coverage', 'Experiment X replicate 2'): src_locked_yml['assemblies'][0]['tracks'][0]['derived'][1]['locked_path'],
                ('DNASeq Coverage', 'Experiment X replicate 2 dna'): src_locked_yml['assemblies'][0]['tracks'][1]['derived'][1]['locked_path'],
            }
        }
        expected_to_swap = {
            'test/path/somewhere_bam': src_locked_yml['assemblies'][0]['tracks'][0]['file']['locked_path'],
            'test/path/somewhere_bam.bai': src_locked_yml['assemblies'][0]['tracks'][0]['derived'][0]['locked_path'],
            'test/path/somewhere_wig': src_locked_yml['assemblies'][0]['tracks'][0]['derived'][1]['locked_path'],
            'test/path/somewhere_else_bam': src_locked_yml['assemblies'][0]['tracks'][1]['file']['locked_path'],
            'test/path/somewhere_else_bam.bai': src_locked_yml['assemblies'][0]['tracks'][1]['derived'][0]['locked_path'],
            'test/path/somewhere_else_wig': src_locked_yml['assemblies'][0]['tracks'][1]['derived'][1]['locked_path'],
        }
        expected_track_paths_mix = {
            'bai': {
                ('RNASeq', 'Experiment X replicate 2'): src_locked_yml['assemblies'][0]['tracks'][0]['derived'][0]['locked_path'],
                ('DNASeq', 'Experiment X replicate 2 dna'): src_locked_yml['assemblies'][0]['tracks'][1]['derived'][0]['locked_path'],
            },
            'bam': {
                ('RNASeq', 'Experiment X replicate 2'): src_locked_yml['assemblies'][0]['tracks'][0]['file']['locked_path'],
                ('DNASeq', 'Experiment X replicate 2 dna'): src_locked_yml['assemblies'][0]['tracks'][1]['file']['locked_path'],
            },
            'gff': {('gff', 'Repeats from REPET'): src_locked_yml['assemblies'][0]['tracks'][2]['file']['locked_path']},
            'wig': {
                ('RNASeq Coverage', 'Experiment X replicate 2'): wig_file.get_usable_path(force_work_dir=True),
                ('DNASeq Coverage', 'Experiment X replicate 2 dna'): wig_file_d.get_usable_path(force_work_dir=True),
            }
        }
        expected_to_swap_mix = {
            'test/path/somewhere_bam': src_locked_yml['assemblies'][0]['tracks'][0]['file']['locked_path'],
            'test/path/somewhere_bam.bai': src_locked_yml['assemblies'][0]['tracks'][0]['derived'][0]['locked_path'],
            'test/path/somewhere_wig': wig_file.get_usable_path(force_work_dir=True),
            'test/path/somewhere_else_bam': src_locked_yml['assemblies'][0]['tracks'][1]['file']['locked_path'],
            'test/path/somewhere_else_bam.bai': src_locked_yml['assemblies'][0]['tracks'][1]['derived'][0]['locked_path'],
            'test/path/somewhere_else_wig': wig_file_d.get_usable_path(force_work_dir=True),
        }
        expected_track_paths_workdir = {
            'bai': {
                ('RNASeq', 'Experiment X replicate 2'): bai_file.get_usable_path(force_work_dir=True),
                ('DNASeq', 'Experiment X replicate 2 dna'): bai_file_d.get_usable_path(force_work_dir=True),
            },
            'bam': {
                ('RNASeq', 'Experiment X replicate 2'): src_locked_yml['assemblies'][0]['tracks'][0]['file']['locked_path'],
                ('DNASeq', 'Experiment X replicate 2 dna'): src_locked_yml['assemblies'][0]['tracks'][1]['file']['locked_path'],
            },
            'gff': {('gff', 'Repeats from REPET'): src_locked_yml['assemblies'][0]['tracks'][2]['file']['locked_path']},
            'wig': {
                ('RNASeq Coverage', 'Experiment X replicate 2'): wig_file.get_usable_path(force_work_dir=True),
                ('DNASeq Coverage', 'Experiment X replicate 2 dna'): wig_file_d.get_usable_path(force_work_dir=True),
            }
        }
        expected_to_swap_workdir = {
            'test/path/somewhere_bam': src_locked_yml['assemblies'][0]['tracks'][0]['file']['locked_path'],
            'test/path/somewhere_bam.bai': bai_file.get_usable_path(force_work_dir=True),
            'test/path/somewhere_wig': wig_file.get_usable_path(force_work_dir=True),
            'test/path/somewhere_else_bam': src_locked_yml['assemblies'][0]['tracks'][1]['file']['locked_path'],
            'test/path/somewhere_else_bam.bai': bai_file_d.get_usable_path(force_work_dir=True),
            'test/path/somewhere_else_wig': wig_file_d.get_usable_path(force_work_dir=True),
        }

        track_paths = ass.get_track_paths()
        assert track_paths == expected_track_paths
        to_swap = ass.jbrowse_track_swapping(fake_jbrowse_json, track_paths)
        assert to_swap == expected_to_swap

        track_paths = ass.get_track_paths(prefer='locked')
        assert track_paths == expected_track_paths

        track_paths = ass.get_track_paths(prefer='workdir')
        assert track_paths == expected_track_paths_workdir

        # Simulate unlocked change
        os.environ['CI_MERGE_REQUEST_LABELS'] = 'run-bam_to_wig'
        Util.mr_labels = find_mr_labels()

        assert wig_file.has_changed_since_last_lock()
        assert not bai_file.has_changed_since_last_lock()
        assert wig_file_d.has_changed_since_last_lock()
        assert not bai_file_d.has_changed_since_last_lock()

        assert wig_file.locked_path == expected_locked_wig
        assert wig_file.get_locked_path() == expected_locked_wig
        assert rnaseq.get_derived_path('wig') != expected_locked_wig
        assert rnaseq.get_derived_path('wig') == wig_file.get_usable_path(force_work_dir=True)
        assert wig_file_d.locked_path == expected_locked_wig_d
        assert wig_file_d.get_locked_path() == expected_locked_wig_d
        assert dnaseq.get_derived_path('wig') != expected_locked_wig_d
        assert dnaseq.get_derived_path('wig') == wig_file_d.get_usable_path(force_work_dir=True)

        track_paths = ass.get_track_paths()
        assert track_paths == expected_track_paths_mix
        to_swap = ass.jbrowse_track_swapping(fake_jbrowse_json, track_paths)
        assert to_swap == expected_to_swap_mix

        track_paths = ass.get_track_paths(prefer='locked')
        assert track_paths == expected_track_paths
        to_swap = ass.jbrowse_track_swapping(fake_jbrowse_json, track_paths)
        assert to_swap == expected_to_swap

        track_paths = ass.get_track_paths(prefer='workdir')
        assert track_paths == expected_track_paths_workdir
        to_swap = ass.jbrowse_track_swapping(fake_jbrowse_json, track_paths)
        assert to_swap == expected_to_swap_workdir

    def test_locked_and_future_locked(self, org_small_future_locked):

        """
        Load locked data AND future locked data
        Simulates what happens after an MR merge, when deploying just after locking
        """

        self.create_fake_derived_files(org_small_future_locked, org_small_future_locked.locked_dir)
        self.create_fake_derived_files(org_small_future_locked, org_small_future_locked.future_locked_dir)

        # Check that the assembly file is in correct shape
        assembly_file = org_small_future_locked.assemblies[0].input_files['fasta']
        jbrowse_file = org_small_future_locked.assemblies[0].derived_files['jbrowse']

        assert assembly_file.get_revision() == 0
        assert assembly_file.locked_revision == 0
        assert jbrowse_file.get_revision() == 0
        assert jbrowse_file.locked_revision == 0

        assert not assembly_file.has_changed_since_last_lock()
        assert not jbrowse_file.has_changed_since_last_lock()

        assert assembly_file.get_usable_path().endswith('/Blabla/Blobloblo/GOGEPP5/10/fasta/10/genomeFUTURE.fa')
        assert assembly_file.get_locked_path().endswith('/Blabla/Blobloblo/GOGEPP5/10/fasta/10/genomeFUTURE.fa')

        assert jbrowse_file.get_usable_path().endswith('/Blabla/Blobloblo/GOGEPP5/10/jbrowse/1.16.11-unknown-0/jbrowseFUTURE.tar.gz')
        assert jbrowse_file.get_locked_path().endswith('/Blabla/Blobloblo/GOGEPP5/10/jbrowse/1.16.11-unknown-0/jbrowseFUTURE.tar.gz')

        assembly_file.revision = 1
        jbrowse_file.revision = 1
        assert assembly_file.has_changed_since_last_lock()
        assert jbrowse_file.has_changed_since_last_lock()

        assert assembly_file.get_usable_path().endswith('/Blabla/Blobloblo/GOGEPP5/10/fasta/10/genomeFUTURE.fa')
        assert assembly_file.get_locked_path().endswith('/Blabla/Blobloblo/GOGEPP5/10/fasta/10/genomeFUTURE.fa')

        assert jbrowse_file.get_usable_path().endswith('/Blabla/Blobloblo/GOGEPP5/10/jbrowse/1.16.11-unknown-0/jbrowseFUTURE.tar.gz')
        assert jbrowse_file.get_locked_path().endswith('/Blabla/Blobloblo/GOGEPP5/10/jbrowse/1.16.11-unknown-0/jbrowseFUTURE.tar.gz')
