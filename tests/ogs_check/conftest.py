from beauris.validation.ogs.ogs_check import OgsCheck

import pytest


@pytest.fixture
def ogs_check(tmp_path_factory):
    ogsc = OgsCheck()

    return ogsc


@pytest.fixture
def temp_dir(tmp_path_factory):

    return tmp_path_factory.mktemp("ogscheck_tmp")


@pytest.fixture
def ogs_check_rna_prefix():
    ogsc = OgsCheck(rna_prefix="mRNA_", adopt_rna_suffix="")

    return ogsc
